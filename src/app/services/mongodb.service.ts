import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import 'rxjs';
import { AssetModel } from '../shared/asset';

const path = '/.netlify/functions/api/asset';

@Injectable({
  providedIn: 'root'
})
export class MongodbService {

  constructor(private http: HttpClient) { }

  getAll(): Promise<any> {
    return this.http
      .get(path).toPromise().then((response: Response) => {
        const result = response;
        return result;
      })
      .catch((error: Response) => throwError(error));
  }

  async regist(asset: AssetModel): Promise<any> {
    return this.http
      .post(path, asset).toPromise().then((response: Response) => {
        const result = response;
        return result;
      })
      .catch((error: Response) => { throwError(error); console.log(error); });
  }
}
