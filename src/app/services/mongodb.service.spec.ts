import { TestBed } from '@angular/core/testing';

import { MongodbService } from './mongodb.service';
import { AppComponent } from '../app.component';
import { HttpClientModule } from '@angular/common/http';

describe('MongodbService', () => {
  let service: MongodbService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [HttpClientModule],
      providers: [HttpClientModule]
    });
    service = TestBed.inject(MongodbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
