import { Component, HostBinding, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { AssetModel } from './shared/asset';
import * as _ from 'underscore';
import { TranslateService } from '@ngx-translate/core';
import { MongodbService } from './services/mongodb.service';
import { Asset } from 'server/models/asset';
import { MatTableDataSource } from '@angular/material/table';
import { ChildActivationStart } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { NativeDateAdapter, DateAdapter } from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'asset-management-sample';

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  selectedLang: string;

  assetForm: FormGroup;

  assets: AssetModel[] = [];
  asset: AssetModel;
  dataSource = new MatTableDataSource<AssetModel>();

  forms = ['partNumber', 'manufacture', 'dataCreater', 'dataLastUpdater', 'calibrationDate'];
  columns = ['partNumber', 'manufacture', 'dataCreater', 'dataLastUpdater', 'calibrationDate', 'edit'];

  constructor(
    private mongodbService: MongodbService,
    private formBuilder: FormBuilder,
    translate: TranslateService,
    dateAdapter: DateAdapter<NativeDateAdapter>
  ) {
    this.createForm();
    translate.setDefaultLang('ja');
    this.selectedLang = 'ja';
    translate.use(this.selectedLang);
    dateAdapter.setLocale('ja-JP');
  }

  ngOnInit() {
    this.getAssets();
  }

  createForm() {
    this.asset = new AssetModel();
    this.assetForm = this.formBuilder.group(this.asset);
  }

  async getAssets() {
    await this.mongodbService
      .getAll()
      .then(async (res: any) => {
        this.assets = res.assets;
        await this.updateList();
      });
  }

  async updateList() {
    this.dataSource = new MatTableDataSource<AssetModel>(this.assets);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  async addAsset() {
    this.asset = this.assetForm.value;
    if (!this.asset) {
      return;
    }
    this.asset._id = '';
    this.asset.__v = '';

    this.mongodbService
      .regist(this.asset)
      .then(async (res: any) => {
        await this.getAssets();
        this.toEdit(res.obj._id);
      });
  }
  async updateAsset() {
    this.asset = this.assetForm.value;
    if (!this.asset) {
      return;
    }

    this.mongodbService
      .regist(this.asset)
      .then(async (res: any) => {
        await this.getAssets();
        this.toEdit(res.obj._id);
      });
  }


  toEdit(id: string): void {
    const newAsset = this.assets.find(a => a._id === id);
    Object.keys(this.asset).map(k =>
      !_.isUndefined(newAsset[k]) ? this.asset[k] = newAsset[k] : this.asset[k] = '');
    this.assetForm = this.formBuilder.group(this.asset);
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

