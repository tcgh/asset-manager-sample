import * as http from 'http';
import { Router, Response } from 'express';
import { Asset } from '../models/asset';
import { AssetModel } from 'src/app/shared/asset';
import { HttpRequest } from '@angular/common/http';
import * as _ from 'underscore';
import { async } from 'rxjs/internal/scheduler/async';
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';

const assetRouter: Router = Router();

// 全てのメッセージを取得する
assetRouter.get('/', (req, res, next) => {
    Asset.find((err, doc) => {
        if (err) {
            return res.status(500).json({
                title: 'エラーが発生しました。',
                error: err.asset
            });
        }

        return res.status(200).json({ assets: doc });
    });
});

// メッセージを登録する
assetRouter.post('/', (req: HttpRequest<AssetModel>, res, next) => {
    // console.log(req.body);
    const newAsset = new Asset(removeNull(req.body));
    if ((!_.isUndefined(req.body._id)) && (req.body._id !== '')) {
        newAsset.isNew = false;
        newAsset.save((err, result) => {
            if (err) {
                Asset.findOne({ _id: req.body._id }).then((asset) => {
                    if (asset) {
                        if (asset.__v !== newAsset.__v) {
                            return res.status(500).json({
                                title: '誰かが更新したようです。',
                                error: err
                            });
                        }
                    }
                    return res.status(500).json({
                        title: 'エラーが発生しました。',
                        error: err
                    });
                });
                return;
            }

            return res.status(200).json({
                asset: '登録しました。',
                obj: result
            });
        });

    } else {
        newAsset.save((err, result) => {
            if (err) {
                return res.status(500).json({
                    title: 'エラーが発生しました。',
                    error: err
                });
            }

            return res.status(200).json({
                asset: '登録しました。',
                obj: result
            });
        });
    }
});

const removeNull = (data) => {
    Object.keys(data).forEach(key => {
        const val = data[key];
        if (val === null || val === '') {
            delete data[key];
        } else if (Array.isArray(val)) {
            val.forEach((v) => {
                this.removeNull(v);
            });
        }
    });
    return data;
};

export { assetRouter };
