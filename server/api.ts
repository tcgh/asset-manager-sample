import * as express from 'express';
import * as path from 'path';
import * as serverless from 'serverless-http';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
require('dotenv').config();

import { assetRouter } from './routes/asset';
import { MONGO_URL } from './config';

export class Api {
    public express: express.Application;


    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }

    private middleware(): void {
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        // 接続する MongoDB の設定
        mongoose.Promise = global.Promise;
        mongoose.connect(process.env.MONGO_URL || MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        process.on('SIGINT', () => { mongoose.disconnect(); });
    }

    private routes(): void {
        this.express.use('/api/asset', assetRouter);
        // netlifyではファイル名がサブディレクトリになる。
        // Expressでは相対パスではなく絶対パスで一致が判断される。
        this.express.use('/.netlify/functions/api/asset', assetRouter);
        // Angularの成果物をexpressでルーティング
        this.express.use(express.static(path.join(__dirname, 'public')));
    }
}

const api = (new Api()).express;
const handler: serverless.Handler = serverless(api);
export { handler };
