import * as mongoose from 'mongoose';
import { AssetModel } from '../../src/app/shared/asset';
import { Document, Model, model, Types, Schema, Query } from 'mongoose';
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';

// Schema
const AssetSchema = Schema({
    partNumber: {
        type: String,
        required: true
    },
    manufacture: String,
    calibrationDate: {
        type: Date
    },
    dataCreater: {
        type: String
    },
    dataLastUpdater: {
        type: String
    }
});

// DO NOT export this
interface IAssetModel extends Document {
    partNumber: string;
    manufacture: string;
    calibrationDate: Date;
    dataCreater: string;

    dataLastUpdater: string;


}

// Virtuals
// UserSchema.virtual('fullName').get(function () {
//     return this.firstName + this.lastName;
// });

// Methods
// UserSchema.methods.getGender = function () {
//     return this.gender > 0 ? 'Male' : 'Female';
// };

// DO NOT export
// interface IUserBase extends IUserSchema {
//     fullName: string;
//     getGender(): string;
// }

// Export this for strong typing
// export interface IUser extends IUserBase {
//     company: ICompany['_id'];
// }

// // Export this for strong typing
// export interface IUser_populated extends IUserBase {
//     company: ICompany;
// }

// Static methods
// UserSchema.statics.findMyCompany = async function (id) {
//     return this.findById(id).populate('company').exec();
// };

// For model
// export interface IUserModel extends Model<IAssetModel> {
//     findMyCompany(id: string): Promise<IUser_populated>;
// }

// Document middlewares
// AssetSchema.pre<IAssetModel>('save', (next) => {
//     // if (this.isModified('password')) {
//     //     // this.password = hashPassword(this.password);
//     // }
// });

// // Query middlewares
// AssetSchema.post<Query<IAssetModel>>('findOneAndUpdate', async (doc) => {
//     // await updateCompanyReference(doc);
// });

// Default export
// export default model<IAssetModel>('User', AssetSchema);
mongoose.plugin(updateIfCurrentPlugin);
const Asset: mongoose.Model<mongoose.Document> = mongoose.model('assets', AssetSchema);

export { Asset };
